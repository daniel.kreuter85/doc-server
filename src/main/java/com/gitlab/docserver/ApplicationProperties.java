package com.gitlab.docserver;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("docserver")
@Data
public class ApplicationProperties {
    private String docTitle;
    private String docPath;

    /**
     * @return normalized path with ending '/', and all '//' replaced by single '/'
     */
    public String getDocPath() {
        return docPath.concat("/").replaceFirst("(/){2,}", "/");
    }
}
