package com.gitlab.docserver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.thymeleaf.templateresolver.FileTemplateResolver;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties.class)
@EnableScheduling
@EnableCaching
public class Application extends WebMvcConfigurerAdapter {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args).registerShutdownHook();
    }

    @Autowired
    private ApplicationProperties docServerProperties;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
            registry.addResourceHandler("/**").addResourceLocations(
                    "file:" + docServerProperties.getDocPath());
    }

    /**
     * @return thymeleaf file template resolver used to access artifact documentation
     */
    @Bean
    public FileTemplateResolver documentationResolver() {
        FileTemplateResolver result = new FileTemplateResolver();
        result.setPrefix(docServerProperties.getDocPath());
        result.setSuffix(".html");
        result.setCacheable(false);
        return result;
    }
}
