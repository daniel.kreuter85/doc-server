package com.gitlab.docserver.api;

import java.util.Collection;

/**
 * Artifact cache definition
 */
public interface Cache {
    /**
     * This method will add a new {@link Group} if no one exists with
     * the given groupId.
     *
     * @param groupId used to lookup the {@link Group}
     * @return group found by it's id
     */
    Group getGroup(String groupId);

    /**
     *
     * @return all groups stored in cache
     */
    Collection<Group> getGroups();
}
