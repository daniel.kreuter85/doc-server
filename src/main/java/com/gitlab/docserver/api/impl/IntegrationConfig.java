package com.gitlab.docserver.api.impl;

import com.gitlab.docserver.ApplicationProperties;
import com.gitlab.docserver.api.Artifact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.annotation.Transformer;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.AcceptOnceFileListFilter;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.integration.transformer.PayloadTypeConvertingTransformer;

import java.io.File;


/**
 * Configure integration logic.
 */
@Configuration
@ComponentScan
public class IntegrationConfig {

    @Autowired
    private ApplicationProperties docServerProperties;

    /**
     * @return the file inbound channel message source
     */
    @Bean
    @InboundChannelAdapter(value = "fileInputChannel", poller = @Poller(fixedDelay = "1000"))
    public MessageSource<File> fileReadingMessageSource() {
        SimplePatternFileListFilter patternFileListFilter = new SimplePatternFileListFilter("index.html");
        AcceptOnceFileListFilter<File> acceptOnceFileListFilter = new AcceptOnceFileListFilter<>();
        CompositeFileListFilter<File> filter = new CompositeFileListFilter();
        filter.addFilters(patternFileListFilter, acceptOnceFileListFilter);
        FileReadingMessageSource source = new FileReadingMessageSource();
        source.setDirectory(new File(docServerProperties.getDocPath()));
        source.setFilter(filter);
        source.setUseWatchService(true);
        return source;
    }

    @Bean
    @Transformer(inputChannel = "fileInputChannel", outputChannel = "cacheInputChannel")
    public PayloadTypeConvertingTransformer<File, ArtifactFileInfo> artifactFileInfoTransformer() {
        PayloadTypeConvertingTransformer<File, ArtifactFileInfo> result = new PayloadTypeConvertingTransformer<>();
        result.setConverter(file -> new ArtifactFileInfo(file));
        return result;
    }
}
