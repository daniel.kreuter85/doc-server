#!/bin/bash

export JAVA_HOME=./jre8

DOC_SERVER_PID="application.pid"
FTP_DAEMON_PID="ftpd-daemon.pid"

# Find JAVA_HOME if not already set
if [ -z "${JAVA_HOME}" ]
 then
    JAVA_HOME=$(dirname $(dirname $(readlink -f $(which java))))
fi

# check the JAVA_HOME path
if [ ! -e ${JAVA_HOME}/bin/java ]
 then
    echo "No java executable found at JAVA_HOME=$JAVA_HOME"
    exit 1
 else
    echo "Found java binary at ${JAVA_HOME}. Continue..."
fi


# check if the pid file exists
check_pid_file(){
 if [ ! -f ${DOC_SERVER_PID} ]
  then
    echo "Doc server is not running."
    exit 1
 fi

 if [ ! -f ${FTP_DAEMON_PID} ]
  then
    echo "FTP server is not running."
    exit 1
 fi
}


# only check doc server process
check_process(){
 if ps -p "$(print_process)" > /dev/null
 then
     return 0
 else
     return 1
 fi
}



# check the status
status(){
    check_pid_file
    if check_process
    then
      echo "$(print_process) is running"
    else
      echo "DOC_SERVER_PID=${DOC_SERVER_PID} does not exist"
    fi
}


# stop the application
stop() {

    if [ ! -f ${DOC_SERVER_PID} ] || ! check_process
    then
      echo "Process "$(print_process)" not running!"
      rm -f ${DOC_SERVER_PID}
      exit 0
    fi

    kill -TERM "$(print_process)"
    kill -TERM $(cat ${FTP_DAEMON_PID})

    echo -ne "Waiting for process to stop"
    KILLED=0
    for i in {1..20}; do
      if [ -f ${DOC_SERVER_PID} ] && check_process
      then
        echo -ne "."
        sleep 1
      else
        KILLED=1
      fi
    done
    echo

    if [ ${KILLED} = 0 ]
    then
      echo "Cannot kill process "$(print_process)" "
      exit 1
    fi

    echo "Process stopped"
    rm -f ${DOC_SERVER_PID}
    rm -f ${FTP_DAEMON_PID}
}



# start the application
start() {
    if [ -f ${DOC_SERVER_PID} ] && check_process
    then
      echo "Process "$(print_process)" already running"
      exit 1
    fi

    echo "nohup ${JAVA_HOME}/bin/java ${JAVA_OPTS} $2 -jar *.jar > ./log/nohup.log 2>&1 &"
    nohup ${JAVA_HOME}/bin/java ${JAVA_OPTS} $2 -jar *.jar > ./log/nohup.log 2>&1 &
    
    echo $! > ${DOC_SERVER_PID}
    echo -e "\nProcess started:" $(print_process) 

    echo "Start ftp server..."
    python ftpd.py &
    echo $! > ${FTP_DAEMON_PID} 
}



# print the pid
print_process(){
    echo "$(cat ${DOC_SERVER_PID})"
}


# evaluate options: start, stop, status ...
evaluate_option() {
    case "$1" in
          status)
                status
            ;;
          stop)
                stop
                exit 0
            ;;
          start)
                start
                exit 0
            ;;
          pid)
                print_process
            ;;
          Quit)
                exit 0
            ;;
          *)
            clear
            echo "Invalid action. Please select between start|stop|status|pid"
    esac
}



# Evaluate the command line argument (script works with and without command line arguments)
if ! [ -z $1 ]
 then
# evaluate command line option
     evaluate_option $1
 else

# show menu
     OPTIONS="status stop start pid Quit"
     select opt in ${OPTIONS}
     do
        evaluate_option ${opt}
     done
fi


