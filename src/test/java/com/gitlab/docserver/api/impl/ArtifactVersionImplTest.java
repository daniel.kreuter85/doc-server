package com.gitlab.docserver.api.impl;

import com.gitlab.docserver.api.ArtifactVersion;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ArtifactVersionImplTest
{
    @Mock
    private ArtifactVersion v1;

    @Mock
    private ArtifactVersion v2;

    @Test
    public void testCompare1() {
        Mockito.when(v1.getVersion()).thenReturn("3.0.0");
        Mockito.when(v2.getVersion()).thenReturn("2.0.0");

        final int result = ArtifactVersionImpl.compareTo(v1, v2);
        assertThat(result).isGreaterThan(0);
    }

    @Test
    public void testCompare2() {
        Mockito.when(v1.getVersion()).thenReturn("1.0.12");
        Mockito.when(v2.getVersion()).thenReturn("1.0.2");

        final int result = ArtifactVersionImpl.compareTo(v1, v2);
        assertThat(result).isGreaterThan(0);
    }

    @Test
    public void testCompare3() {
        Mockito.when(v1.getVersion()).thenReturn("1.0.2-SNAPSHOT");
        Mockito.when(v2.getVersion()).thenReturn("1.0.2");

        final int result = ArtifactVersionImpl.compareTo(v1, v2);
        assertThat(result).isLessThan(0);
    }
}