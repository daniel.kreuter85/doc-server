# Documentation Server

The documentation server is based on the spring boot platform.
The server scans the file system for new documentation artifacts. Beaware that 
the server only looks for index.html file.
A documentation artifact is specified by groupId, artifactId and version like maven does.
The server provides a user frontend that displays the documentations.


## Installation 

* The application requires Java 8
* Compile the java source code
* Download pyftpdlib (https://github.com/giampaolo/pyftpdlib)
* Create a directory doc-server on the target host
* Add a application.properties file (see /src/main/resources)
* Add the start/stop script application.sh (see /src/main/scripts)
* Add the python ftp server script ftpd.py (see /src/main/scripts)
* Add the pyftpdlib directory that contains the python scripts
* Add the docserver-xxx.jar
* Create a subdirectory with name *docs*. Default ftp server directory


## FTP Server Credentials
The credential defaults are:

* User: doc-ftp-server
* Password: doc-ftp-server-password


## Customizing

### Documentation directory
To change the location for your documentations, you need to change:

* docserver.doc-path in application.properties (properties for the doc server itself)
* ftpd.py script contains the same path (look for lines that contain authorizer.add_...)

### FTP Server
You may customize the ftpd.py script to use another username and password or another doc directory for the ftp server